function showMenu() {
    document.getElementById('content').style.overflowY = 'hidden';
    document.getElementById('open-menu').style.opacity = '0';
    document.getElementById('menu-overlay').style.cssText = 'opacity: 1; z-index: 100;'

    setTimeout(function () {
        document.getElementById('open').style.display = 'none';
        document.getElementById('close').style.display = 'block';
        document.getElementById('menu-content').style.opacity = '1';
    }, 1000);

    setTimeout(function () {
        document.getElementById('close-menu').style.opacity = '1';
    }, 1100);
}

function hideMenu() {
    document.getElementById('content').style.overflowY = 'auto';
    document.getElementById('close-menu').style.opacity = '0';
    document.getElementById('menu-content').style.opacity = '0';

    setTimeout(function () {
        document.getElementById('close').style.display = 'none';
        document.getElementById('open').style.display = 'block';
        document.getElementById('menu-overlay').style.cssText = 'opacity: 0, z-index: unset;';
    }, 1000);

    setTimeout(function () {
        document.getElementById('open-menu').style.opacity = '1';
    }, 1100);
}